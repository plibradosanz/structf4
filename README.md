# Struct-f4: a Rcpp package for ancestry profile and population structure inference from f4-statistics

The f4 statistics has become increasingly popular in the last decade, not only as a test of admixture, but also as informing on the levels of genetic drift shared between pairs of individuals or populations. By measuring allele sharing only, the f4 statistics dismisses with the signal exclusive to each individual or population, including population-specific drift and sporadic inbreeding, which are well-known to distorting PCA-like and clustering-based analyses. The number of f4 permutations, however, grows rapidly with the number of analysed genomes. This involves computational and analytical challenges, since calculating and interpreting all the resulting information under a single framework might thus become impracticable with previous computational tools.

To circumvent these drawbacks, we here present Struct-f4 package, able to characterise the population structure underlying a set of samples from their corresponding f4 permutations, and ultimately providing f4-based results in the form of Multi-Dimensional Scaling as well as characterising the ancestry components underlying each individual.

# Installation
The Struct-f4 package includes three main scripts, Struct-f4.r, Calc-f4 and Calc-f3. The first is written in Rcpp, and does not require any installation as it compiles on-the-fly. It however depends on a series of R packages that need to be pre-installed. These are:
 - Rcpp
 - data.table
 - RcppZiggurat
 - gplots
 - ggplot2
 - this.path
 - R.utils
 - ggrepel
 - ape
 - optparse

which could be easily installed in a R terminal:

    install.packages(c('Rcpp', 'data.table', 'RcppZiggurat', 'gplots', 'ggplot2', 'this.path', 'R.utils', 'ggrepel', 'ape', 'optparse'),dependencies=T)

# Compiling and Running Calc-f4 & Calc-f3
The two supplementary C scripts, Calc-f4.c and Calc-f3.c, are aimed at rapidly calculating f4 and f3 statistics, the former of which are required as input for Struct-f4. Calc-f3 is provided for consistency, but note that the f3 statistics are not used as input for Struct-f4. To compile these two C scripts, type in a Unix terminal:

    gcc Calc-f4.c -lpthread -lm -lz -Wall -o Calc-f4 && chmod 755 Calc-f4
    gcc Calc-f3.c -lpthread -lm -lz -Wall -o Calc-f3 && chmod 755 Calc-f3

The Calc-f4 running options are:

    ./Calc-f4 -h
    Usage of Calc-f4:
    Calc-f4 -h [help and exit]
            -i [input file list, with each line pointing to a treemix-like file containing the allele count data per jackknife block]
            -o [column (integer) representing the outgroup in the allele count files, to fix it as H4 in the permutations (optional!)]
            -n [number of taxa included]
            -t [number of threads to parallelize calculations, so that each thread performs calculations within a subset of the allele count files]

As examples, a total of 20 TreeMix-like files are included within the *examples/* subfolder. These are small subset, to illustrate the input format required by Calc-f4. Each one summarises the allele count data within 5Mb-long jackknife blocks, collectively encompassing ~5% of the human genome. They are not randomly distributed, but represent the first 100Mb of chromosome 1. These 20 files are listed in *examples/calcf4.filelist*.  More specifically, the three first lines of one of these files are:

    head -n 3 3_vscenarioM_paper_0.25_0.00_3_3_a0.25_b0.00_S3.8.3.out_genetic
    COUNTS 0_0 0_1 0_2 5_15 5_16 5_17 7_21 7_22 7_23 9_27 9_28 9_29 10_30 10_31 10_32 12_36 12_37 12_38 13_39 13_40 13_41 14_42 14_43 14_44 15_45 15_46 15_47 17_51 17_52 17_53
    1 0,1 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0 0,1 0,1 0,1 0,1 1,0 1,0 1,0 1,0 0,1 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0 1,0
    1 0,1 1,0 1,0 0,1 1,0 0,1 1,0 1,0 1,0 1,0 1,0 0,1 1,0 1,0 0,1 1,0 1,0 0,1 1,0 1,0 0,1 0,1 0,1 1,0 1,0 0,1 1,0 0,1 0,1 0,1
    2 0,1 1,0 0,1 1,0 0,1 0,1 1,0 0,1 0,1 1,0 0,1 1,0 0,1 1,0 1,0 1,0 1,0 0,1 1,0 1,0 1,0 0,1 1,0 1,0 0,1 0,1 0,1 0,1 0,1 1,0

and indicate that these files are space-delimited, where the header is comprised of the keyword *COUNTS*, followed by the ids of the 30 samples (e.g. in this example, 0_0, 0_1, and 0_2 are the three individuals sampled from the simulated population 0). The following rows include the allele counts for each individual. These can be expressed as *ancestral, derived*, or *major, minor*, or *reference, alternative*, it does not matter. As this example comes from coalescent simulations of haplotype sequences, each individual solely contains 0s and 1s, but other ploidies and missing data (e.g. *0,0*) are also possible. The individuals were simulated according to the evolutionary history illustrated in Fig. 1, by Harney and colleagues (2021), and assuming an introgression from population 14a into 14 of alpha = 25%, which took place 40 generations ago. **Important note**: The first column indicates how many SNPs of each type are within the jackknife b\
lock. This considerably reduces computational costs, as positions with the same allele counts across all individuals are processed only once.

To run Calc-f4:

    ./Calc-f4 -i examples/calcf4.filelist -n 30 -t 4 | gzip -f > examples/calcf4.f4.gz

or

    ./Calc-f4 -i examples/calcf4.filelist -n 30 -t 4 -o 19 | gzip -f > examples/calcf4.f4.gz

if we aim at conditioning on the subset of permutations including sample *13_40* as outgroup. The columns in the output file *examples/calcf4.f4.gz* indicate the samples involved in the permutation, (H1, H2; H3, H4), the corresponding number of BABA and ABBA events, the total number of SNPs, the f4-statistics and its bias-corrected version, together with its standard error and Z-score.

**> Do you need help to generate your input files?**
This package additionally includes a Perl script, Tped2Structf4.pl, to convert a tped and a tfam plink file into multiple 5Mb-long files, in Struct-f4 format. To run it:
	
	perl Tped2Structf4.pl file.tped file.tfam 5000000 output_blocks/file 1 
	
The first and the second argument represent paths to the corresponding tped and tfam files, as similarly defined in format by plink. The only peculiarity pertains to the tfam file, as the first column should be the sample name, while its sixth (last) column may contain group labels to cluster individuals into populations (the format of columns 2-5 does not matter, as the Perl script kind of ignores their content). As a general rule, however, grouping individuals into populations is not recommended, since Struct-f4 learns what are the ancestral groups only if multiple samples per population are provided as separate individuals. If you want to treat each sample as a separate individual (recommended), the content of the sixth column in the tfam file should be the same as that in column 1 (ie. the sample id).
Appart from that peculiar the tfam file, the third argument in the Perl command is the block length for jackknifing. The fourth argument specifies the stem, common root name, for the multiple 5Mb-long output files. The last option indicates whether the data should be treated as pseudohaploid (0 or 1 values are accepted). If pseudohaploid (1), the allele counts calculated from the tped file are divided by two, as the tped format was originally designed to store information of diploid genotypes, and not haplotypes.

# Running Struct-f4
Once dependencies are successfully installed, and f4 permutations calculated, you are ready to run Struct-f4. Unless the -V option is supplied, Struct-f4 will consecutively run two Markov chains. The former aims at estimating the shared genetic drift between each pair of individuals, for subsequent Multi-Dimensional Scaling. This further helps clustering individuals into *K* ancestry components, and thus to generate a very reasonable starting value for the second Markov chain, which models individuals as potential mixtures of these *K* ancestral groups. The first iterations of the second MCMC might be slow, when Struct-f4 conducts a throughout exploration of the parametric space.

The options *-m* and *-n* specify the corresponding number of MCMC iterations desired, while the option *-t* indicates a thinning interval for both MCMC chains. The burning period (*-b*) is only relevant for the second MCMC, as Struct-f4 only considers the best iteration for the first MCMC.

To see all available options, together with their corresponding descriptions:



        Usage: Struct-f4.r [options]

        Options:
        -f CHARACTER, --f4file=CHARACTER
                Path to gzipped file with f4 permutations, as provided by Calcf4

        -T INTEGER, --threads=INTEGER
                Number of threads during calculations [default= 4]

        -K INTEGER, --ancestries=INTEGER
                Number of K ancestral components to consider

        -p CHARACTER, --pdf=CHARACTER
                Path to the plots that will be generated by Struct-f4 (pdf) [default= plots.pdf]

        -m INTEGER, --mcmc1_iter=INTEGER
                Iterations for the 1st MCMC chain (assuming no admixture) [default= 1000000]

        -n INTEGER, --mcmc2_iter=INTEGER
                Iterations for the 2nd MCMC chain (assuming admixture) [default= 5000000]

        -t INTEGER, --mcmc_thinin=INTEGER
                Thinin interval for the both MCMC chains (assuming no admixture) [default= 100]

        -b INTEGER, --mcmc2_burnin=INTEGER
                Burnin interval for the 2nd MCMC chain (number of MCMC2 samples to disregard before summarising the posterior distributions) [default= 250000]. No equivalent option needed for mcmc1

        -c CHARACTER, --constraints=CHARACTER
                Path to the tabulated file that contains the constraints (optional!)

        -s INTEGER, --seed=INTEGER
                Seed for random number generation (optional!)

        -v CHARACTER, --start_val1=CHARACTER
                Use this option to specify a file with parameters from a previous MCMC1 run, including one parameter per line. This will start by re-runing MCMC1 using those values specified in the file. Useful if previous run was not converged (optional!)

        -V CHARACTER, --start_val2=CHARACTER
                Use this option to specify a file with parameters from a previous MCMC2 run, including one parameter per line. This will directly re-run MCMC2 using those values specified in the file. Useful if previous run was not converged (optional!)

        -q CHARACTER, --qpAdm=CHARACTER
                Specify sample ids (separated by :) to be modelled as a mixture of the remaining individuals, the latter of which will be automatically clustered into K ancestral components upfront, hence representing reference populations (optional!)

        -h, --help
                Show this help message and exit

In Struct-f4, the minimum running command requires the input file, including the f4 permutations calculated from Calc-f4 (*-f* option), the number of ancestries (*-K*) and the output files, which will be both in the form of plain text (to the standard output) and as a collection of summary pdf plots (*-p*). Additionally, as this small data set could possibly reach convergence rapidly, we might consider reducing the number of MCMC1 iterations (*-m*).

    Rscript Struct-f4.r -f examples/calcf4.f4.gz -K 7 -p examples/calcf4.f4.plots.pdf -m 200000 > examples/calcf4.f4.out

The summary plots in *examples/calcf4.f4.plots.pdf* include the likelihood trajectories of both Markov chains (pages 1 and 3), a Multi-Dimensional Scaling plot from MCMC1 (page 2), as well as a bar plot with the individual ancestry profiles from MCMC2 (page 5), together with genetic drift shared between the *K* ancestry components (page 4).

The plots in *examples/calcf4.f4.plots.pdf* show that individuals are well separated by population, but ancestry proportions could appear to be noisy with only three individuals per population. Note, however, that the average introgression inferred across the three individuals of population 14 was 19.7%, instead of the simulated 25%. It could be that MCMC chains were not converged. If it were the case, to avoid restarting the analyses from the beginning, users could alternatively extract the MCMC iteration showing the best likelihood score from the previous run, stored in *examples/calcf4.f4.out*, to then redirect the associated parameters to another file (one parameter per line). This file can be used together with the *-v*/*-V* options to resume either the Markov chain from those values. In your Unix terminal, a command equivalent to this would extract the best parameters from the second MCMC chain:

    grep Iter2 examples/calcf4.f4.out  | perl -p -e 's/ /\t/g' | sort -k17,17gr | head -n 1 | perl -p -e 's/.+Parameters\t=\t//g' | perl -p -e 's/\t/\n/g' > examples/init.parameters2resumeMCMC2.tsv

to then re-run Struct-f4.r

    Rscript Struct-f4.r -f examples/calcf4.f4.gz -K 7 -p examples/calcf4.f4.plots.pdf -V examples/init.parameters2resumeMCMC2.tsv > examples/calcf4.f4.resumed.out

MCMC chains had however already converged in the previous run, as the likelihood profile in *examples/calcf4.f4.plots.pdf* remains steady, showing no evidence of further improvement (average introgression, still 18.91%). Another potential validation could involve to assess whether the amount of information included in only 20 jackknife blocks was enough (~5% of the human genome). Recall that these are in addition a non-random representation, but simply the first ~100Mb of chromosome 1. While facing low informativeness, it might be recommendable to semi-supervise inference to check for consistency, and constraint the ancestry proportions of some individuals, such as those belonging to unadmixed *reference* populations. In Struct-f4, this can be done through two complementary options. The *-c* option expects a tabulated file, where the first column indicates the sample IDs, followed by *K* additional columns containing the proportion of ancestry attributed to each *K* component. A value of -1 informs to Struct-f4 that a given ancestry proportion is unknown, and thus needs to be treated as a parameter, to be estimated. Note the K = 9, as the constraints file is here forcing Struct-f4 to distinguish between populations 0, 5 (the closest surrogate to the true donor population) and 7. With such constraint, the average introgression closely approaches to the simulated one (23.3%).

    Rscript Struct-f4.r -f examples/calcf4.f4.gz -K 9 -p examples/calcf4.f4.plots.constrained.pdf -c examples/constraints.tsv  > examples/calcf4.f4.constrained.out

The second option, *-q*, expects a list of sample IDs separated by colon symbols. All f4 permutations including these sample IDs are temporarily filtered out, before running the first MCMC chain, which thus clusters the remaining individuals (ideally unadmixed) into populations. The resulting *K* ancestral clusters are fixed as reference populations in the second MCMC chain, which aims at modelling the "queried" samples  (those supplied by *-q*) as mixtures of the previously fixed *K* reference populations. The second MCMC automatically stops once converged (no likelihood improvement observed in a few thousand MCMC iterations).

    Rscript Struct-f4.r -f examples/calcf4.f4.gz -K 7 -p examples/calcf4.f4.plots.queried.pdf -q 14_42:14_43:14_44 > examples/calcf4.f4.queried.out

Both *-q* and *-c* can be jointly combined:

    Rscript Struct-f4.r -f examples/calcf4.f4.gz -K 9 -p examples/calcf4.f4.plots.constraintedAndQueried.pdf -q 14_42:14_43:14_44 -c examples/constraints.tsv > examples/calcf4.f4.constrainedAndQueried.out


# Further considerations: tips and caveats

Despite Calcf4 and Structf4.r are comparatively fast, users might experience memory limitations from a few hundred samples. In such situations, alternatively consider leveraging a subset of f4 permutations, such as fixing a distant outgroup as H4 through the *-o* option in Calcf4.

Struct-f4 characterises the ancestry composition that persists in the sampled genomes, and not the migration rate that took place some generations ago. These differ in the fraction of introgressed ancestry that is lost every generation by genetic drift or other evolutionary forces, following a highly stochastic process.

The assumption that the f4 statistics approximately follows a Normal distribution is common practice but can and must be also questioned. This is especially true in the presence of recent admixture or highly drifted populations showing limited incomplete lineage sorting. The calculated likelihood does not represent the true probability of the parameters, but an approximation, and this bias often leads to underestimates of the migration rate.

It is also true that, depending on the population history, even moderate amounts of gene flow might barely alter the f4 statistics, leaving no detectable (or noisy) trace of gene flow in this metrics. Excluding some populations or even some f4 permutations, and conducting semi-supervised inference, through the constraints file (*-c* option) or querying specific samples (*-q*), might help mitigate all these problems by reducing the parametric space, but critical interpretation of the results is always best recommendation.



# Questions and/or suggestions?
Write me an email to plibradosanz@gmail.com