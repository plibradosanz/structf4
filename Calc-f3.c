#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <pthread.h>
#include <math.h>
struct permutation {
  int p1;
  int p2;
  int p3;
  int p4;
  double baba;
  double abba;
  double tot;
  //  double rest;
};

struct associ {
  char *p1;
  char *p2;
  char *p3;
  char *p4;
};

unsigned int *blockSize;

long seed=0;
long randomv=0;
pthread_mutex_t count_mutex;
unsigned long int perm;
unsigned int n_files;
unsigned int n_taxa;
unsigned int *start;
unsigned int *end;
char **files;
double ***blocks;
int outgroup = -1;
struct associ *associat; 

double comb (double n, double k)
{
  double prod=1;
  double cnt=0;
  double i;
  for (i=k;i>=1;i--){
    prod*=(n-cnt)/i;
    if (prod<0){
      abort();
    }

    cnt++;
  }
  return prod;
}

char **read_paths (char *path, unsigned int *n_files){
  char *line;
  FILE *paths_file;
  size_t len=5000;

  line = (char*)malloc(len);
  if (line == NULL){
    fprintf(stderr,"\tERROR: Cannot allocate memory for line\n");
    exit(1);
  }
  memset(line,'\0',len);

  paths_file=fopen(path,"r");
  if (paths_file == NULL){
    fprintf(stderr,"\tERROR: Unable to open %s\n",path);
    exit(1);
  }

  char **paths=NULL;
  unsigned int cnt=1;
  while (getline(&line, &len, paths_file) != -1) {
    if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
      continue;

    line[strlen(line)-1]='\0';
    if (line[strlen(line)-1] == '\r')
      line[strlen(line)-1]='\0';

    if (cnt == 1){
      paths=(char **)malloc(sizeof(char *));
    }else{
      paths=(char **)realloc(paths,sizeof(char *)*cnt);
    }
    paths[cnt-1]=(char *)malloc(sizeof(char)*strlen(line)+1);
    strcpy(paths[cnt-1],line);
    cnt++;
  }
  *n_files=cnt-1;
  free (line);
  fclose(paths_file);
  return (paths);
}

char **ExtractArray(char **files, unsigned int start, unsigned int end){
  char **subarray=(char **)malloc(sizeof(char *)*(end-start+1));
  unsigned int i;
  for (i=start;i<end;i++){
    unsigned int index=i-start;
    subarray[index]=(char *)malloc(sizeof(char )*(strlen(files[i])+1));
    strcpy(subarray[index],files[i]);
  }
  subarray[end-start]=NULL;
  return (subarray);
}

void *doCalculations (void *arg){
  unsigned int myid = *((unsigned int *)arg);
  //  fprintf(stderr,"pointer2; %p %u\n",arg,myid);
  char *line;
  unsigned int cnt, p1, p2, p3, i,begin,final;
  size_t len=5000;

  pthread_mutex_lock(&count_mutex);
  begin=start[myid];
  final=end[myid];
  pthread_mutex_unlock(&count_mutex);

  struct permutation *perms;
  perms=(struct permutation *)malloc(perm*(sizeof(struct permutation)));
  if (perms == NULL){
    fprintf(stderr,"No memory for some many permutations\n");
    exit (0);
  }
  line = (char*)malloc(len);
  if (line == NULL){
    fprintf(stderr,"\tERROR: Cannot allocate memory for line\n");
    exit(1);
  }
  unsigned int size;
  for (size = begin; size<final; size++){
    FILE *treemix;
    pthread_mutex_lock(&count_mutex);
    treemix=fopen(files[size],"r");
    //    fprintf (stderr, "FILES: %u: %u %u %u %s\n",myid,begin,final, size,files[size]);
    pthread_mutex_unlock(&count_mutex);
    if (treemix == NULL){
      fprintf(stderr,"\tERROR: Unable to open %s\n",files[size]);
      exit(1);
    }
    memset(line,'\0',len);
    
    unsigned long int p=0;
    unsigned int p1e=n_taxa;
    unsigned int p2e=n_taxa-1;
    unsigned int p3e=n_taxa;
    unsigned int p3s;
    for (p1=0; p1<p1e;p1++){
      for (p2=0; p2<p2e;p2++){
	if (p2==p1)
          continue;
	p3s=p2+1;;
	for (p3=p3s; p3<p3e;p3++){
	  if (p3==p1)
            continue;
          perms[p].p1=p1;
          perms[p].p2=p2;
          perms[p].p3=p1;
          perms[p].p4=p3;
          perms[p].abba=0;
          perms[p].baba=0;
          perms[p].tot=0;
          p++;
	}
      }
    }
    char **labelsm;
    labelsm=(char **)malloc(sizeof(char *));

    unsigned line_len;

    //read header
    getline(&line, &len, treemix);
    if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
      continue;

    line_len=strlen(line)-1;
    line[line_len]='\0';
    if (line[line_len] == '\r')
      line[line_len]='\0';

    cnt=0;
    char *end_field;
    char *field = strtok_r(line, " ",&end_field);
    while (field != NULL){
      field = strtok_r (NULL, " ", &end_field);
      if (field == NULL)
	break;
      labelsm[cnt]=(char *)malloc(sizeof(char)*strlen(field)+1);
      strcpy(labelsm[cnt],field);
      labelsm=(char **)realloc(labelsm,sizeof(char *)*(cnt+2));
      cnt++;
    }

    double ab_a[n_taxa][n_taxa];
    double ba_a[n_taxa][n_taxa];
    double gen[n_taxa];
    double oneminusp1, ab, ba;

    unsigned int count,sum, a1, a2;
    char *end_allele;
    char *freq1;
    char *freq2;
    //read data
    unsigned int bsize=0;
    while (getline(&line, &len, treemix) != -1) {
      if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
	continue;

      line_len=strlen(line)-1;
      line[line_len]='\0';
      if (line[line_len] == '\r')
	line[line_len]='\0';

      field = strtok_r(line, " ",&end_field);
      count=atoi(field);

      cnt=0;
      while (field != NULL){

	field = strtok_r (NULL, " ", &end_field);
	if (field == NULL)
	  break;

	freq1 = strtok_r (field, ",", &end_allele);
	freq2 = strtok_r (NULL, ",", &end_allele);
	a1=atoi(freq1);
	a2=atoi(freq2);
	sum=a1+a2;
	if (sum>0){
	  gen[cnt]=a1/(double)(sum);
	}else{
	  gen[cnt]=2;
	}
	cnt++;
      }
      bsize+=count;
      for (p1=0; p1<n_taxa-1;p1++){
	oneminusp1=1.0-gen[p1];
	for (p2=(p1+1); p2<n_taxa;p2++){
	  if (gen[p2] == 2 || gen[p1] == 2)
	    continue;
	  ab=gen[p1]*(1.0-gen[p2]);
	  ba=oneminusp1*gen[p2];
	  ab_a[p1][p2]=ab;
	  ab_a[p2][p1]=ba;
	  ba_a[p1][p2]=ba;
	  ba_a[p2][p1]=ab;
	}
      }
      
      //f3
      p=0;
      double ab1,ba1;
      double ab2,ba2,abbatmp,baabtmp,babatmp,ababtmp;
      for (p1=0; p1<n_taxa;p1++){
        for (p2=0; p2<p2e;p2++){
          if (p2==p1)
            continue;
          ab1=ab_a[p1][p2]*count;
          ba1=ba_a[p1][p2]*count;
          p3s=(p2+1);
          for (p3=p3s; p3<n_taxa;p3++){
            if (p3==p1)
              continue;
            if (gen[p3] == 2 || gen[p2] == 2 || gen[p1] == 2){
              p++;
              continue;
            }
            ab2=ab_a[p1][p3];
            ba2=ba_a[p1][p3];
            abbatmp=ab1*ba2;
            baabtmp=ba1*ab2;
            babatmp=ba1*ba2;
            ababtmp=ab1*ab2;
            perms[p].abba+=(abbatmp + baabtmp);
            perms[p].baba+=(babatmp + ababtmp);;
            perms[p].tot+=count;
            p++;
          }
        }
      }
    }
    fclose(treemix);
    
    pthread_mutex_lock(&count_mutex);
    blockSize[size]=bsize;
    for (p = 0; p<perm;p++){
      blocks[p][0][size]=(double)(perms[p].abba);
      blocks[p][1][size]=(double)(perms[p].baba);
      blocks[p][2][size]=(double)(perms[p].tot);
      if (size == 0){
	char *p1=labelsm[perms[p].p1];
	char *p2=labelsm[perms[p].p2];
	char *p3=labelsm[perms[p].p3];
	char *p4=labelsm[perms[p].p4];
	associat[p].p1=(char *)malloc((strlen(p1)+1)*sizeof(char));
	associat[p].p2=(char *)malloc((strlen(p2)+1)*sizeof(char));
	associat[p].p3=(char *)malloc((strlen(p3)+1)*sizeof(char));
	associat[p].p4=(char *)malloc((strlen(p4)+1)*sizeof(char));
	strcpy(associat[p].p1,p1);
	strcpy(associat[p].p2,p2);
	strcpy(associat[p].p3,p3);
	strcpy(associat[p].p4,p4);
      }
    }
    pthread_mutex_unlock(&count_mutex);
    for (i=0;i<n_taxa;i++)
      free (labelsm[i]);
    free (labelsm);
  }
  free (perms);
  free (line);
  return NULL;
}
 
void *doCalculationsOutgroupFixed (void *arg){
  unsigned int myid = *((unsigned int *)arg);
  //  fprintf(stderr,"pointer2; %p %u\n",arg,myid);
  char *line;
  unsigned int cnt, p1, p2, p3, i,begin,final;
  size_t len=5000;

  pthread_mutex_lock(&count_mutex);
  begin=start[myid];
  final=end[myid];
  pthread_mutex_unlock(&count_mutex);

  struct permutation *perms;
  perms=(struct permutation *)malloc(perm*(sizeof(struct permutation)));
  if (perms == NULL){
    fprintf(stderr,"No memory for some many permutations\n");
    exit (0);
  }

  line = (char*)malloc(len);
  if (line == NULL){
    fprintf(stderr,"\tERROR: Cannot allocate memory for line\n");
    exit(1);
  }
  unsigned int size;
  for (size = begin; size<final; size++){
    FILE *treemix;
    pthread_mutex_lock(&count_mutex);
    treemix=fopen(files[size],"r");
    pthread_mutex_unlock(&count_mutex);
    if (treemix == NULL){
      fprintf(stderr,"\tERROR: Unable to open %s\n",files[size]);
      exit(1);
    }

    memset(line,'\0',len);

    p1=outgroup;
    unsigned long int p=0;
    unsigned int p2e=n_taxa-1;
    unsigned int p3e=n_taxa;
    unsigned int p3s;
    for (p2=0; p2<p2e;p2++){
      if (p2 == p1)
	continue;
      p3s=p2+1;
      for (p3=p3s; p3<p3e;p3++){
	if (p3==p1)
	  continue;
	perms[p].p1=p1;
	perms[p].p2=p2;
	perms[p].p3=p1;
	perms[p].p4=p3;
	perms[p].abba=0;
	perms[p].baba=0;
	perms[p].tot=0;
	p++;
      }
    }


    unsigned int line_len;
    char **labelsm;
    labelsm=(char **)malloc(sizeof(char *));

    //read header
    getline(&line, &len, treemix);
    if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
      continue;

    line_len=strlen(line)-1;
    line[line_len]='\0';
    if (line[line_len] == '\r')
      line[line_len]='\0';

    cnt=0;
    char *end_field;
    char *field = strtok_r(line, " ",&end_field);
    while (field != NULL){
      field = strtok_r (NULL, " ", &end_field);
      if (field == NULL)
        break;
      labelsm[cnt]=(char *)malloc(sizeof(char)*strlen(field)+1);
      strcpy(labelsm[cnt],field);
      if (cnt == outgroup)
	fprintf(stderr,"The outgroup is %s\n",labelsm[cnt]);
      labelsm=(char **)realloc(labelsm,sizeof(char *)*(cnt+2));
      cnt++;
    }

    double ab_a[n_taxa];
    double ba_a[n_taxa];
    double gen[n_taxa];

    unsigned int count,sum, a1, a2;
    char *end_allele;
    char *freq1;
    char *freq2;
    unsigned int bsize=0;
    while (getline(&line, &len, treemix) != -1) {
      if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
        continue;
      
      line_len=strlen(line)-1;
      line[line_len]='\0';
      if (line[line_len] == '\r')
        line[line_len]='\0';
      
      field = strtok_r(line, " ",&end_field);
      count=atoi(field);      
      cnt=0;
      
      while (field != NULL){
	field = strtok_r (NULL, " ", &end_field);
	if (field == NULL)
	  break;
	
	freq1 = strtok_r (field, ",", &end_allele);
	freq2 = strtok_r (NULL, ",", &end_allele);
	a1=atoi(freq1);
	a2=atoi(freq2);
	sum=a1+a2;
	if (sum>0){
	  gen[cnt]=a1/(double)(sum);
	}else{
	  gen[cnt]=2;
	}
	cnt++;
      }
      bsize+=count;

      p1=outgroup;
      double oneminusp1=1.0-gen[p1];
      for (p2=0; p2<n_taxa;p2++){
        if (p1 == p2 || gen[p2] == 2 || gen[p1] == 2)
	  continue;
	double oneminusp2=1.0-gen[p2];
	double ab=gen[p1]*oneminusp2;
	double ba=oneminusp1*gen[p2];
	ba_a[p2]=ba;
	ab_a[p2]=ab;
      }


      //f3
      p=0;
      for (p2=0; p2<n_taxa-1;p2++){
        if (p2==p1)
          continue;
        double ab1=ab_a[p2]*count;
        double ba1=ba_a[p2]*count;
        unsigned int p3s=(p2+1);
        for (p3=p3s; p3<n_taxa;p3++){
          if (p3==p1)
            continue;
          if (gen[p3] == 2 || gen[p2] == 2 || gen[p1] == 2){
            p++;
            continue;
          }
          double ab2=ab_a[p3];
          double ba2=ba_a[p3];
          double abbatmp=ab1*ba2;
          double baabtmp=ba1*ab2;
          double babatmp=ba1*ba2;
          double ababtmp=ab1*ab2;
          perms[p].abba+=(abbatmp + baabtmp);
          perms[p].baba+=(babatmp + ababtmp);
          perms[p].tot+=count;
          p++;
        }
      }
    }
    fclose(treemix);

    pthread_mutex_lock(&count_mutex);
    blockSize[size]=bsize;
    for (p = 0; p<perm;p++){
      blocks[p][0][size]=(double)(perms[p].abba);
      blocks[p][1][size]=(double)(perms[p].baba);
      blocks[p][2][size]=(double)(perms[p].tot);
      if (size == 0){
	char *p1=labelsm[perms[p].p1];
	char *p2=labelsm[perms[p].p2];
	char *p3=labelsm[perms[p].p3];
        char *p4=labelsm[perms[p].p4];
	associat[p].p1=(char *)malloc((strlen(p1)+1)*sizeof(char));
	associat[p].p2=(char *)malloc((strlen(p2)+1)*sizeof(char));
	associat[p].p3=(char *)malloc((strlen(p3)+1)*sizeof(char));
	associat[p].p4=(char *)malloc((strlen(p4)+1)*sizeof(char));
	strcpy(associat[p].p1,p1);
	strcpy(associat[p].p2,p2);
	strcpy(associat[p].p3,p3);
	strcpy(associat[p].p4,p4);
      }
    }
    pthread_mutex_unlock(&count_mutex);
    for (i=0;i<n_taxa;i++)
      free (labelsm[i]);
    free (labelsm);
  }
  free (perms);
  free (line);
  return NULL;
}

void FreeSubArray(char **array,unsigned int start, unsigned int end){
  unsigned int i;
  for (i=start;i<end;i++)
    free(array[i]);
  free (array);
}
void usage(void)
{
  printf("\nUsage of Calc-f3:\n");

  printf("\nCalc-f3 -h [help and exit]\n");
  printf("          -i [input file list, with each line pointing to a treemix-like file containing the allele count data per jackkniffe block]\n");
  printf("          -o [column (integer) representing the outgroup in the allele count files, to calculate outgroup-f3 permutations]\n");
  printf("          -n [number of taxa included]\n");
  printf("          -t [number of threads to parallelize calculations, so that each thread performs calculations within a subset of the allele count files]\n");
  printf("\n");
}

int main(int argc, char *argv[]) {

  char input[1024];
  unsigned int structf4 = 0;

  int threads;
  unsigned int i;
  unsigned long int p;

  for(i=0;i<argc;i++)
    fprintf(stderr,"%s ",argv[i]);

  int arg;
  if( argc > 1 ){
    arg = 1;
    while( arg < argc ){
      if( argv[arg][0] != '-' ){
        if(argv[arg][0] == '>')
          break;
        fprintf(stderr,"\nERROR: Argument should be -%s ?\n", argv[arg]);
        exit(1);
      }

      switch (argv[arg][1]){
      case 'i':
        arg++;
        sscanf(argv[arg], "%s",input);
        break;

      case 'o':
        arg++;
        sscanf(argv[arg], "%i",&outgroup);
        break;

      case 'n':
        arg++;
        sscanf(argv[arg], "%i",&n_taxa);
        break;

      case 't':
        arg++;
        sscanf(argv[arg], "%i",&threads);
        break;

      case 's':
	arg++;
	sscanf(argv[arg], "%i",&structf4);
	break;

      case 'r':
	arg++;
	sscanf(argv[arg], "%lu",&randomv);
	break;

      case 'h' :
        usage();
        exit(0);
        break;

      default:
        exit(1);
        break;

      }
      arg++;
    }
  }else{
    exit(1);
  }	
  
  seed=randomv;
  fprintf(stderr,"\n%lu (%p) %lu (%p)\n",randomv,&randomv,seed,&seed);
  
  if (outgroup == -1){
    perm = n_taxa*(n_taxa-1)*(n_taxa-2)/(double)2.0;
  }else{
    perm = (n_taxa-1)*(n_taxa-2)/(double)2.0;
  }
  files=read_paths(input, &n_files);

  fprintf(stderr,"There are %i files, involving %lu f4-stat permutations each. This analysis will be executed in %i threads\n",n_files, perm, threads);

  blockSize = (unsigned int*)malloc(n_files * sizeof(unsigned int));

  associat = (struct associ*)malloc(perm * sizeof(struct associ));
  blocks = (double ***)malloc(perm * sizeof(double **));
  for (p=0;p<perm;p++){
    blocks[p] = (double **)malloc(3 * sizeof(double *));
    for (i=0;i<3;i++){
      blocks[p][i] = (double *)malloc(n_files * sizeof(double));
    } 
  }

  //divide the n_files in nthreads
  pthread_t thread[threads];
  unsigned int works_per_thread=(unsigned int)(n_files/(double)threads);
  unsigned int works_per_thread1=works_per_thread+1;
  unsigned int tot_threads=threads*works_per_thread;
  unsigned int remaining=n_files-tot_threads;
  if (works_per_thread<1){
    fprintf(stderr,"The number of threads cannot be greater than the number of blocks (files) in %s",input);
    exit(1);
  }
  start=(unsigned int *)malloc(sizeof(unsigned int)*threads);
  end=(unsigned int *)malloc(sizeof(unsigned int)*threads);
  unsigned int indexes[threads];
  start[0]=0;
  end[0]=start[0]+works_per_thread;
  indexes[0]=0;
  fprintf(stderr,"\n%i %i\n",remaining, threads);
  for (i= 1; i<remaining;i++){
    start[i]=end[i-1];
    end[i]=start[i]+works_per_thread1;
    indexes[i]=i;
  }
  for (i= remaining; i<threads;i++){
    if (i>0){
      start[i]=end[i-1];
    }else{
      start[i]=0;
    }
    end[i]=start[i]+works_per_thread;
    indexes[i]=i;
  }
  if (end[threads-1] < n_files)
    end[threads-1]=n_files;

  for (i=0; i<threads;i++)
    fprintf(stderr,"%u %u %u\n",i,start[i],end[i]);

  for (i= 0; i<threads;i++){
    int result;
    if (outgroup == -1){
      result=pthread_create(&thread[i], NULL, doCalculations, (void *)&indexes[i] );
    }else{
      result=pthread_create(&thread[i], NULL, doCalculationsOutgroupFixed, (void *)&indexes[i] ); //the function of indexes here is crucial, to ensure every i has a different memory address.
    }
    if (result !=0){
      fprintf(stderr,"Error creating thread %u , exit code: %i\n",i,result);
      exit(1);
    }
  }
  fprintf (stderr,"DONE\n");
  /*Joining all threads*/
  for(i = 0; i < threads; i++)
      pthread_join(thread[i], NULL);

  free (start);
  free (end);

  if (structf4 == 1){
    for (i=0; i<2; i++){
      for (p=0; p<perm ;p++){
	double sum_den=0;
	double sum_num=0;
	double s=0;
	double wmean=0;
	double bf[n_files];
	double etai[n_files];
	unsigned int j;
	unsigned numsnps=0;
	for (j=0; j<n_files; j++){
	  sum_den+=blocks[p][2][j];
	  sum_num+=blocks[p][i][j];
	  numsnps+=blockSize[j];
	}
	double f=sum_num/(double)sum_den;
	for (j=0; j<n_files; j++){
	  bf[j]=blockSize[j]/(double)numsnps;
	  etai[j]=(sum_num-blocks[p][i][j])/(double)(sum_den-blocks[p][2][j]);
	  wmean+=etai[j]*bf[j];
	  s+=f-etai[j];
	}
	double jest=s+wmean;
	double sd=0;
	unsigned int tot=n_files;
	for (j=0; j<n_files; j++){
	  if (bf[j]>0){
	    double r=1.0/(double)bf[j];
	    double xt=r*f;
	    double w2=(r-1)*etai[j];
	    double s1=xt-w2;
	    double s2=s1-jest;
	    double sq=s2*s2;
	    double s=sq/(double)(r-1);
	    sd+=s;
	  }else{
	    //	  fprintf(stderr,"FILE %i has no SNPs, permutation %lu?\n",j,p);
	    tot--;
	  }
	}
	sd/=tot;
	sd=sqrt(sd);
	double zscore=-1;
	if (sd>0)
	  zscore=jest/(double)sd;
	
	if (i == 0){
	  fprintf(stdout,"%s\t%s\t%s\t%s\t0.0\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\tABBA\n",associat[p].p1,associat[p].p2,associat[p].p3,associat[p].p4,sum_num,sum_den,f,jest,sd,zscore);
	}else if (i == 1){
	  fprintf(stdout,"%s\t%s\t%s\t%s\t%.20f\t0.0\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\tBABA\n",associat[p].p1,associat[p].p2,associat[p].p3,associat[p].p4,sum_num,sum_den,f,jest,sd,zscore);
	}
	//else if (i == 2){
	//	  fprintf(stdout,"%s\t%s\t%s\t%s\t%.20f\t0.0\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\tREST\n",associat[p].p1,associat[p].p2,associat[p].p3,associat[p].p4,sum_num,sum_den,f,jest,sd,zscore);
	//	}
      }
    }
  }
  //else{
    for (p=0; p<perm ;p++){
      double sum_den=0;
      double sum_num=0;
      double s=0;
      double wmean=0;
      double bf[n_files];
      double etai[n_files];
      double baba=0;
      double abba=0;
      unsigned int numsnps=0;
      unsigned int j;
      for (j=0; j<n_files; j++){
	sum_den+=blocks[p][2][j];
	sum_num+=blocks[p][1][j] - blocks[p][0][j];
	baba+=blocks[p][1][j];
	abba+=blocks[p][0][j];
	numsnps+=blockSize[j];
      }
      double f=sum_num/(double)sum_den;
      for (j=0; j<n_files; j++){
	bf[j]=blockSize[j]/(double)numsnps;
	etai[j]=(sum_num- (blocks[p][1][j] - blocks[p][0][j]))/(double)(sum_den-blocks[p][2][j]);
	wmean+=etai[j]*bf[j];
	s+=f-etai[j];
      }
      double jest=s+wmean;
      double sd=0;
      unsigned int tot=n_files;
      for (j=0; j<n_files; j++){
	if (bf[j]>0){
	  double r=1.0/(double)bf[j];
	  double xt=r*f;
	  double w2=(r-1)*etai[j];
	  double s1=xt-w2;
	  double s2=s1-jest;
	  double sq=s2*s2;
	  double s=sq/(double)(r-1);
	  sd+=s;
	}else{
	  tot--;
	}
      }
      sd/=tot;
      sd=sqrt(sd);
      double zscore=-1;
      if (sd>0)
	zscore=jest/(double)sd;

      fprintf(stdout,"%s\t%s\t%s\t%s\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\t%.20f\tf4\n",associat[p].p1,associat[p].p2,associat[p].p3,associat[p].p4,baba,abba,sum_den,f,jest,sd,zscore);
    }
    //}

    for (i= 0; i<n_files;i++)
      free(files[i]);
  free (files);
  free (blockSize);
  

  for (p=0;p<perm;p++){
    for (i=0;i<3;i++){
      free (blocks[p][i]);
    }
    free (blocks[p]);
    free (associat[p].p1);
    free (associat[p].p2);
    free (associat[p].p3);
    free (associat[p].p4);

  }  
  free (associat);
  free (blocks);
  return 1;
}


