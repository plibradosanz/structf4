#include <Rcpp.h>
// [[Rcpp::depends(RcppParallel)]]
#include <RcppParallel.h>
#include <Rcpp/Benchmark/Timer.h>
// [[Rcpp::depends(RcppZiggurat)]]
#include <Ziggurat.h>

using namespace Rcpp;

static Ziggurat::Ziggurat::Ziggurat zigg;
const double MIN_D=1e-50;
const double MAX_D=1-1e-50;

struct LKLParallel : public RcppParallel::Worker
{
  // source vector
  const RcppParallel::RMatrix<double> input;
  const RcppParallel::RMatrix<int> p_sub;
  const RcppParallel::RMatrix<double> dsub;

  // accumulated value output
  double LKL;
  // constructors
  LKLParallel(const NumericMatrix input, const IntegerMatrix p_sub, const NumericMatrix dsub) : input(input), p_sub(p_sub), dsub(dsub), LKL(0.0) {}
  LKLParallel(const LKLParallel& lklparallel, RcppParallel::Split) : input(lklparallel.input),  p_sub(lklparallel.p_sub), dsub(lklparallel.dsub), LKL(0.0) {}

  // accumulate just the element of the range I've been asked to
  void operator()(std::size_t begin, std::size_t end) {
    double f4;
    double f4o;
    std::size_t e;
    for (e = begin; e < end; e++){
      RcppParallel::RMatrix<int>::Row p_sub_row = p_sub.row(e);
      RcppParallel::RMatrix<double>::Row dsub_row = dsub.row(e);
      f4o=input(p_sub_row[0],p_sub_row[1])*input(p_sub_row[2],p_sub_row[3]);
      f4=(f4o-dsub_row[0])*dsub_row[1];
      LKL+=f4*f4;
    }
  }

  // join my value with that of another Sum
  void join(const LKLParallel& rhs) {
    LKL += rhs.LKL;
  }
};

struct DistanceAncestriesParallel : public RcppParallel::Worker {
  const RcppParallel::RMatrix<double> Ds;
  const RcppParallel::RMatrix<double> Qs;
  RcppParallel::RMatrix<double> out_sub;
  unsigned int K;
  unsigned int len;

  DistanceAncestriesParallel(const NumericMatrix Ds, const NumericMatrix Qs,NumericMatrix out_sub, unsigned int K, unsigned int len) : Ds(Ds), Qs(Qs), out_sub(out_sub), K(K), len(len){}

 void operator()(std::size_t begin, std::size_t end) {
    std::size_t i,j,t,z;
    double tmp,tmp2;
    for (i = begin; i<end; i++){
      for (j = (i+1); j<len; j++){
        tmp=0;
        for (t = 0; t<K; t++){
          if (Qs(i,t)>0){
            tmp2=0;
            for (z = 0; z<K; z++)
              tmp2+=Ds(t,z)*Qs(j,z);
            tmp+=tmp2*Qs(i,t);
          }
        }
        out_sub(i,j) = tmp;
        out_sub(j,i) = out_sub(i,j);
      }
      out_sub(i,i) = 0;
    }
  }
};

  // [[Rcpp::export]]
NumericMatrix DistanceAncestries(NumericMatrix Ds,NumericMatrix Qs,  unsigned int K,unsigned int len){
  NumericMatrix out_sub(len,len);
  unsigned int i;
  for (i=0;i<K;i++)
    Ds(i,_)=abs(Ds(i,_));

  DistanceAncestriesParallel DistanceAncestriesParallel(Ds, Qs,out_sub, K, len);
  parallelFor(0,len-1,DistanceAncestriesParallel,3);

  return (out_sub);
}

// [[Rcpp::export]]
void mdistNoErrPrint(NumericMatrix out_rest, IntegerMatrix p_sub, NumericMatrix dsub, unsigned int long s) {
  double f4;
  double f4o;
  std::size_t e;
  Rcpp::Rcout.precision(18);
  for (e = 0; e < s; e++){
    IntegerVector p_sub_row = p_sub(e,_);
    NumericVector dsub_row = dsub(e,_);
    f4o=out_rest(p_sub_row[0],p_sub_row[1])*out_rest(p_sub_row[2],p_sub_row[3]);
    f4=(f4o-dsub_row[0])*dsub_row[1];
    Rcout << e << ": " << f4 << " (" << f4o << " - " << dsub_row[0] << ") * " << dsub_row[1] << "\n";
  }
}

// [[Rcpp::export]]
double mdistNoErrFast(NumericMatrix out_rest, IntegerMatrix p_sub, NumericMatrix dsub, unsigned int long s, bool Beta) {
  double lkl;
  LKLParallel lklParallel(out_rest, p_sub, dsub);
  parallelReduce(0, s, lklParallel);     
  lkl=R::dnchisq(lklParallel.LKL,s,dsub(0,3),1);

  if (!std::isfinite(lkl)){
    return (-999999999999999999999999999999999999999999999999.0);
  }else{
    return (lkl);
  }
}

// [[Rcpp::export]]
double bin2param (int bin){
  return (std::pow(1.0625,bin));
}

// [[Rcpp::export]]
int param2bin (double param){
  if (param>0){
    double tmp=(std::log(param)/(double)0.06062462181643484258061);
    int int_tmp=int(tmp);
    double check=bin2param(int_tmp);
    if (check>param)
      int_tmp--;
    return int_tmp;
  }else{
    return 1;
  }
}

// [[Rcpp::export]]
NumericMatrix GaussianMutationMH_Ds2 (NumericMatrix mutate, NumericMatrix scaling, unsigned int K){

  unsigned int i,j;
  double proposal1;
  NumericMatrix mutate_n(K,K);
  unsigned int cnt;

  for (i=0;i<K;i++){
    for (j=i;j<K;j++){
      cnt=0;
      do{
	if (cnt>=3 && scaling(i,j)<0.5)
	  scaling(i,j)*=2;
	proposal1=mutate(i,j) + R::rnorm(0,scaling(i,j)); //zigg.norm()*scaling(i,j);
	cnt++;
      }while(proposal1<(-MAX_D) || proposal1>MAX_D);
      
      mutate_n(i,j)=proposal1;
      mutate_n(j,i)=proposal1;
    }
  }

  return (mutate_n);
}


// [[Rcpp::export]]
NumericMatrix GaussianMutationMH_Qs (NumericMatrix mutate, IntegerMatrix scaling, unsigned int K, unsigned int len, NumericMatrix constraints){

  unsigned int i,j;
  NumericMatrix mutate_n=clone(mutate); 
  NumericMatrix mutatebin(len,K);
  for (i=0;i<len;i++){
    for (j=0;j<K;j++)
      mutatebin(i,j)=param2bin(mutate(i,j));
  }
  
  for (i=0;i<len;i++){
    IntegerVector sca=scaling(i,_);
    if (sca[1]>-251){
      unsigned int donnor=0;
      for (j=0;j<K;j++){
	if (constraints(i,j) == -1){
	  if (mutatebin(i,j)>=sca[j] && mutatebin(i,j)<1)
	      donnor++;
	}
      }
      unsigned int rd;
      if (donnor>0){
	IntegerVector vdonnor(donnor);
	unsigned int cnt_donnor=0;
	for (j=0;j<K;j++){
          if (constraints(i,j) == -1){
	    if (mutatebin(i,j)>=sca[j] && mutatebin(i,j)<1)
	      vdonnor[cnt_donnor++]=j;
          }	
	}
	
	rd=std::floor(R::runif(0,cnt_donnor));

	int change=sca[vdonnor[rd]];
	unsigned int receptor=0;
	for (j=0;j<K;j++){
	  if (constraints(i,j) == -1 && j!=(unsigned int)vdonnor[rd]){
	    if (param2bin(1-mutate(i,j))>=change && mutate(i,j)<1)
              receptor++;
	  }
	}
	if (receptor>0){
	  IntegerVector vreceptor2(receptor);
	  unsigned int cnt_receptor=0;
	  for (j=0;j<K;j++){
	    if (constraints(i,j) == -1 && j!=(unsigned int)vdonnor[rd]){
	      if (param2bin(1-mutate(i,j))>=change && mutate(i,j)<1)
		vreceptor2[cnt_receptor++]=j;
	    }
	  }
	  unsigned int rr=std::floor(R::runif(0,cnt_receptor));
	  double change2=bin2param(change);
	  mutate_n(i,vdonnor[rd])-=change2;
	  mutate_n(i,vreceptor2[rr])+=change2;
	}//  else{
	//   Rcout << scaling << "\n";
	//   Rcout << rd << " " << change << "\n";

	//   for (j=0;j<K;j++){
	//     if (constraints(i,j) == -1){
	//       if (mutatebin(rd,j)>=sca[j] && mutatebin(rd,j)<1)
	// 	Rcout << j << " " << mutate(rd,j) << " " <<  " " << param2bin(mutate(rd,j)) << "\n";
	//     }
	//   }

	//   for (j=0;j<K;j++){
	//     if (constraints(i,j) == -1 && j!=rd){
	//       Rcout << j << " " << mutate(i,j) << " " << 1-mutate(i,j) << " " << param2bin(1-mutate(i,j)) << "\n";
	//     }
	//   }
	//   Rcout << vdonnor << "\n";
	//   Rcout << mutate << "\n" << mutate_n << "\n";
	//   Rcpp::stop ("debug\n");
	// }
	
      }
    }
  }
  return (mutate_n);
}


// [[Rcpp::export]]
double prob_norm (double p, double scaling, double max, double min){
  return ( (std::erf((max-p)/(double)scaling) - std::erf((min-p)/(double)scaling))/(double)2.0 );
}

// [[Rcpp::export]]
double transition_p (NumericVector param, NumericVector param_n, IntegerVector scaling, unsigned int K2,  NumericVector constraints){
  unsigned int j;
  unsigned int donnor=0;
  unsigned int donnor2=0;
  unsigned int receptor=0;
  unsigned int diff=0;

  for (j=0;j<K2;j++){
    if (constraints[j] == -1){
      int param1=param2bin(param[j]);
      int param2=param2bin(param_n[j]);
      if (param1>=scaling[j] && param[j]>0)
	donnor++;
      if (param1!=param2){
	diff++;
	if (param1>param2 && param1<1)
	  donnor2=j;
      }
    }
  }
  double p=1.0;
  if (diff>0){
    int change=scaling[donnor2];
    for (j=0;j<K2;j++){
      if (constraints[j] == -1 && j!=donnor2){
	if (param2bin(1-param[j])>=change && param[j]<1)
	  receptor++;
      }
    }
    if (receptor==0)
      return (1e-120);

    if (donnor == 0){

      Rcpp::Rcout.precision(42);

      Rcout << "=================\n";
      Rcout << param << "\n" << param_n << "\n" << scaling << "\n" << p << "\n";
      Rcout << donnor << " " << receptor << " " << diff << "\n";
      for (j=0;j<K2;j++){
	if (constraints[j] == -1){
	  int param1=param2bin(param[j]);
	  int param2=param2bin(param_n[j]);
	  	  double tmp=std::log(param[j])/(double)0.06062462181643484258061;
		  //double tmp=std::log(param[j])/(double)6.103329368063852491316E-5;
	  Rcout << param[j] << " " << param1 << " " << scaling[j] << " " << std::log(param[j]) << " " << tmp << " " << std::floor(tmp) << "\n";
	  if (param1>=scaling[j] && param[j]>0)
	    Rcout << "\tDonnor!\n";
	  if (param1!=param2){
	    if (param1>param2 && param1<1)
	      Rcout << "\t" << j << "\n";
	  }
	}
      }
      stop ("ERROR\n");
    }else{
      p=1.0/(double)(donnor*receptor);
    }
  }
  
  if (p==0){
    Rcpp::Rcout.precision(42);
    Rcout << "=================\n";
    Rcout << param << "\n" << param_n << "\n" << scaling << "\n" << p << "\n";
    Rcout << donnor << " " << receptor << " " << diff << "\n";
    stop ("ERROR\n");
  }
  return std::log(p);

}


struct GetProposals_DsParallel2 : public RcppParallel::Worker {
  const RcppParallel::RMatrix<double> param;
  const RcppParallel::RMatrix<double> param_n;
  const RcppParallel::RMatrix<double> scaling;
  const unsigned int K;
  double proposal0;
  double proposal1;

  GetProposals_DsParallel2(const NumericMatrix param, const NumericMatrix param_n,const NumericMatrix scaling, const unsigned int K) : param(param),param_n(param_n),scaling(scaling),K(K), proposal0(0.0), proposal1(0.0){}
  GetProposals_DsParallel2(const GetProposals_DsParallel2& GetProposals_Dsparallel2,  RcppParallel::Split) : param(GetProposals_Dsparallel2.param), param_n(GetProposals_Dsparallel2.param_n), scaling(GetProposals_Dsparallel2.scaling), K(GetProposals_Dsparallel2.K), proposal0(0.0), proposal1(0.0){}

  void operator()(std::size_t begin, std::size_t end) {
    std::size_t i,j;

    for (i=begin;i<end;i++){
      for (j=i;j<K;j++){
	double den=scaling(i,j)*1.414213562373095145475; 
	proposal0+=std::log( prob_norm(param(i,j), den, MAX_D, -MAX_D) );
	proposal1+=std::log( prob_norm(param_n(i,j), den, MAX_D, -MAX_D) );
      }
    }
  }
  void join(const GetProposals_DsParallel2& rhs) {
    proposal0+= rhs.proposal0;
    proposal1+= rhs.proposal1;
  }
};


// [[Rcpp::export]]
NumericVector GetProposals_Ds2 (NumericMatrix param, NumericMatrix param_n, NumericMatrix scaling, unsigned int K){

  NumericVector proposals(2);
  GetProposals_DsParallel2 GetProposals_DsParallel_o(param,param_n,scaling,K);
  parallelReduce(0, K, GetProposals_DsParallel_o);

  proposals[0]=GetProposals_DsParallel_o.proposal0;
  proposals[1]=GetProposals_DsParallel_o.proposal1;
  return (proposals);
}


// [[Rcpp::export]]
NumericVector GetProposals_Qs (NumericMatrix param, NumericMatrix param_n, IntegerMatrix scaling, unsigned int K, unsigned int len,  NumericMatrix constraints){
  unsigned int i;
  NumericVector proposals(2);

  for (i=0;i<len;i++){
    IntegerVector sca=scaling(i,_);
    if (sca[1]>-251){
      NumericVector pa=param(i,_);
      NumericVector pa_n=param_n(i,_);
      NumericVector cons=constraints(i,_);
      proposals[0]+=transition_p(pa,pa_n,sca,K,cons);
      proposals[1]+=transition_p(pa_n,pa,sca,K,cons);
    }
  }

  return (proposals);
}

// [[Rcpp::export]]
double accept2scale(double a_rate, double scale, double llimit, double target){
  if (a_rate>1)
    a_rate=1;
  double factor=std::exp(5*(target-a_rate)); 
  
  if (factor < 1 || (scale>llimit && factor>1)){
    double tmp=scale/(double)factor;
    if (tmp>0.25){
      return (scale);
    }else{
      return (tmp);
    }
  }else{
    return (scale);
  }
}

// [[Rcpp::export]]
NumericMatrix mcmc (NumericMatrix freq, unsigned long int burnin, unsigned int thinin, unsigned long int iter, IntegerMatrix posnames, NumericMatrix dsub, NumericMatrix paramK, unsigned int len, unsigned int K, bool Beta){


  unsigned int j,z;
  unsigned long int i;
  unsigned int cnt=0;
  unsigned int record=0;

  unsigned int accept=0;
  double lik;
  unsigned int nparam=K*K;
  unsigned int toRecord=(int)(iter-burnin)/thinin-1;
  NumericMatrix out(toRecord, nparam+2);
  unsigned int cnt_scale=0;
  
  double scale=1e-5;
  NumericMatrix scaling(K,K);
  NumericMatrix scalingE(K,K);

  for (i=0;i<K;i++){
    for (j=0;j<K;j++){
      scaling(i,j)=1e-5;
      scalingE(i,j)=1e-5;
    }
  }

  Rcout << "lik\n";
  NumericMatrix Ds=DistanceAncestries(freq, paramK, K, len);

  unsigned int long s=(unsigned int long)dsub.nrow();
  lik=mdistNoErrFast(Ds,posnames, dsub,s, Beta);
  Rcout << lik << "\n";
  
  NumericMatrix freq_n(K,K);
  NumericVector proposals(2);
  double lik_n;
  double ratio;
  unsigned int b=0;
  double a_rate;
  unsigned int cnt_index;
  NumericVector PrintParam(nparam);
  for (i=1;i<=iter;i++){
    freq_n=GaussianMutationMH_Ds2(freq, scaling, K); 
    proposals=GetProposals_Ds2(freq, freq_n, scaling, K);

    Ds=DistanceAncestries(freq_n, paramK, K, len);
    lik_n=mdistNoErrFast(Ds,posnames, dsub,s, Beta);

    ratio=lik_n + proposals[0] - lik - proposals[1];
    
    // //    if (K != len){
      //     Rcpp::Rcout.precision(18);
      //     Rcout << "===================\n" << lik_n << " + " << proposals[0] << " - " << lik << " - " << proposals[1] << " = " << ratio << "\n\n";
      // Rcout << Ds << "\n\n" << freq << "\n\n" << freq_n <<"\n\n";
      //    }
    //       Rcpp::Rcout.precision(18);
    b=0;
    if (ratio>=0){
      b=1;
    }else{
      if (std::log(R::runif(0,1))<=ratio)
	b=2;
    }
    if (b == 1){
      for (j=0;j<K;j++){
        for (z=j; z<K; z++)
          scalingE(j,z)=std::fabs(freq_n(j,z)-freq(j,z));
      }
    }else{
      for (j=0;j<K;j++){
        for (z=j; z<K; z++)
          scalingE(j,z)=1e-10;
      }
    }
    
    if (b >0){
      freq=freq_n;
      lik=lik_n;
      accept++;
    }
    for (j=0; j<K; j++){
      for (z=j; z<K; z++){
	     scaling(j,z)=scalingE(j,z) + std::fabs(zigg.norm()*scale);
        
	     if (scaling(j,z)<1e-10)
	       scaling(j,z)=1e-10;
      }
    }

    if (cnt == thinin){
      a_rate=accept/(double)thinin;
      accept=0;
      if (cnt_scale>100 || record<3){
        scale=R::runif(0.00001,0.01);
        cnt_scale=0;
      }else{
	scale=accept2scale(a_rate,scale,1e-8, 0.05+R::rexp(0.05));
        cnt_scale++;
      }
      if (i>burnin && record<toRecord){
	out(record,0)=i;
	out(record,1)=lik;
	cnt_index=2;
	for (j=0;j<K;j++){
	  for (z=0; z<K; z++)
	    out(record,cnt_index++)=freq(j,z);
	}

	cnt_index=0;
	for (j=0;j<K;j++){
          for (z=0; z<K; z++)
	    PrintParam[cnt_index++]=freq(j,z);
	}

	Rcout << "Iter1 = " << i << std::scientific << "; Noise = " << scale <<  "; Acceptance = "<< a_rate << "; lkl = " << std::fixed  << lik << "; Parameters = " << PrintParam << "\n";
	record++;
      }
      cnt=0;
    }
    cnt++;
  }
  return out;
}

// [[Rcpp::export]]
int anyTwo(NumericVector x, unsigned int K){
  unsigned int i;
  int tot=0;
  for (i=0;i<K;i++){
    if (x[i]==-1){
      tot++;
    }
  }

  if (tot>1){
    return 1;
  }
  return 0;
}


// [[Rcpp::export]]
NumericMatrix mcmc4 (NumericMatrix freq, NumericMatrix Qs,  unsigned long int burnin, unsigned int thinin, unsigned long int iter, IntegerMatrix posnames, NumericMatrix dsub, unsigned int K, unsigned int len, NumericMatrix constraints, bool Beta){

  unsigned int j,z;
  unsigned long int i;
  unsigned int cnt=0;
  double lik;

  unsigned int accept=0;
  double scale=1e-5;

  NumericMatrix scaling_Frq(K,K);
  NumericMatrix scalingE_Frq(K,K);
  for (i=0;i<K;i++){
    for (j=0;j<K;j++){
      scaling_Frq(i,j)=1e-5;
      scalingE_Frq(i,j)=1e-5;
    }
  }

  NumericMatrix Ds=DistanceAncestries(freq, Qs,  K, len);
  unsigned int long s=(unsigned int long)dsub.nrow();
  lik=mdistNoErrFast(Ds, posnames, dsub,s, Beta);

  NumericMatrix freq_n(K,K);
  double lik_n;
  NumericVector proposals_Ds(2);
  double ratio;
  unsigned int b;
  double a_rate;

  int convergence=0;
  double ml=lik;
  NumericMatrix bestDs=Ds;
  unsigned int cnt_ml=0;

  i=0;
  while(convergence==0){
    freq_n=GaussianMutationMH_Ds2(freq,scaling_Frq, K);
    Ds=DistanceAncestries(freq_n, Qs, K, len);
    lik_n=mdistNoErrFast(Ds,posnames, dsub,s, Beta);
    proposals_Ds=GetProposals_Ds2(freq, freq_n, scaling_Frq, K);

    ratio=lik_n +  proposals_Ds[0] - lik - proposals_Ds[1];

    b=0;
    if (ratio>=0){
      b=1;
    }else{
      if (std::log(R::runif(0,1))<=ratio)
        b=2;
    }

    if (b == 1){
      for (j=0;j<K;j++){
        for (z=j; z<K; z++){
          scalingE_Frq(j,z)=std::fabs(freq_n(j,z)-freq(j,z));
        }
      }
      if (lik_n>ml){
	ml=lik_n;
	bestDs=freq_n;
	cnt_ml=0;
      }
    }
    if (b>0){
      freq=freq_n;
      lik=lik_n;
      if ((b ==1 && ratio>1e-6) || (b==2 && ratio<(-1e-6)))
	accept++;
    }
    for (j=0; j<K; j++){
      double noise=zigg.norm()*scale;
      for (z=j; z<K; z++){
	scaling_Frq(j,z)=scalingE_Frq(j,z) + noise;

	if (scaling_Frq(j,z)<MIN_D)
	  scaling_Frq(j,z)=MIN_D;
      }
    }

    cnt_ml++;
    if (cnt_ml>=20*K*K*len)
      convergence=1;

    if (cnt == thinin){
      a_rate=accept/(double)thinin;
      scale=accept2scale(a_rate,scale,1e-10, 0.05+R::rexp(0.05));
      accept=0;
      cnt=0;
    }
    cnt++;
    i++;
  }
  return bestDs;
}

// [[Rcpp::export]]
NumericMatrix mcmc3 (NumericMatrix freq, NumericMatrix Qs,  unsigned long int burnin, unsigned int thinin, unsigned long int iter, IntegerMatrix posnames, NumericMatrix dsub, unsigned int K, unsigned int len, NumericMatrix constraints, bool Beta){

  unsigned int j,z;
  unsigned long int i;
  unsigned int cnt=0;
  double lik;

  unsigned int accept=0;
  double scale=1e-4;
  unsigned int cnt_scale=0;

  IntegerMatrix scaling_Qs(len,K);
  NumericMatrix scalingE_Qs(len,K);
  for (i=0;i<len;i++){
    for (j=0;j<K;j++){
      scaling_Qs(i,j)=-100;
      scalingE_Qs(i,j)=1e-4;
    }
  }

  NumericMatrix Ds=DistanceAncestries(freq, Qs,  K, len);
  unsigned int long s=(unsigned int long)dsub.nrow();
  lik=mdistNoErrFast(Ds, posnames, dsub,s, Beta);

  NumericMatrix Qs_n(len,K);
  double lik_n;
  NumericVector proposals_Qs(2);
  double ratio;
  unsigned int b;
  double sd;
  int sd2;
  double tochange=1;
  double minToChange=1/(double)len;
  double maxToChange=1;
  int convergence=0;
  double ml=lik;
  NumericMatrix bestQs=Qs;
  unsigned int cnt_ml=0;
  double ilik=lik;
  
  if (maxToChange>1)
    maxToChange=1;
   i=0;
  while(convergence==0){
    Qs_n=GaussianMutationMH_Qs(Qs,scaling_Qs, K, len, constraints);
    Ds=DistanceAncestries(freq, Qs_n, K, len);
    lik_n=mdistNoErrFast(Ds,posnames, dsub,s, Beta);
    proposals_Qs=GetProposals_Qs(Qs, Qs_n, scaling_Qs, K, len, constraints);

    ratio=lik_n +  proposals_Qs[0] - lik - proposals_Qs[1];

    b=0;
    if (ratio>=0){
      b=1;
    }else{
      if (std::log(R::runif(0,1))<=ratio)
        b=2;
    }
    // if ((b ==1 && ratio>1e-6)){
    //   Rcpp::Rcout.precision(12);
    //   Rcout << "===================\n" << std::fixed << lik_n << " + " << proposals_Qs[0] << " - " << lik << " - " << " - "  << proposals_Qs[1] << " = " << ratio << "\n";
    //   Rcout << Qs << "\n\n" << Qs_n << "\n\n";
    // }

    if (b>0){
      if (b==1){
        for (j=0;j<len;j++){
	      NumericVector diff(K);
          for (z=0; z<K; z++)
            diff[z]=Qs_n(j,z)-Qs(j,z);
          double maxc=max(diff);
          double maxz=max(Qs_n(j,_));
          if (maxc>maxz)
            maxc=maxz;
          for (z=0; z<K; z++)
            scalingE_Qs(j,z)=maxc;
	    }

	    if (lik_n>ml){
	      if (lik_n-ml>50)
	        cnt_ml=0;
	      ml=lik_n;
	      bestQs=Qs_n;
	    }
      }else{
        for (j=0;j<len;j++){
          for (z=0;z<K;z++)
            scalingE_Qs(j,z)=1e-6;
        }
      }

      Qs=Qs_n;
      lik=lik_n;
      if ((b ==1 && ratio>1e-6) || (b==2 && ratio<(-1e-6)))
	  accept++;
    }
    unsigned int cnt2=0;
    tochange=1;
    if (tochange<minToChange){
      tochange=minToChange;
    }else if (tochange>maxToChange){
      tochange=maxToChange;
    }
    double tmp=R::runif(0,1);
    if (tmp<0.5){
      for (j=0;j<len;j++){
	if (R::runif(0,1)<tochange && anyTwo(constraints(j,_),K)==1){
	  sd=scalingE_Qs(j,0) + zigg.norm()*scale;
	  double maxc=max(Qs(j,_));
	  if(sd>maxc)
	    sd=maxc;
	  if (sd<1e-5)
	    sd=1e-5;
	    
	  sd2=param2bin(sd);
	  cnt2++;

	  for (z=0; z<K; z++)
	    scaling_Qs(j,z)=sd2;
	}else{
	  for (z=0; z<K; z++)
	    scaling_Qs(j,z)=-251;
	}
      }
    }else if (tmp<0.9){
      unsigned int cnt_c=0;
      do{
	z=std::floor(R::runif(0,K));
	cnt_c++;
      }while(anyTwo(constraints(_,z),len)==0 && cnt_c<K);

      cnt_c=0;
      double tmp;
      unsigned int cnt_js;
      do{
	cnt_js=0;
	for (j=0;j<len;j++){
	  tmp=R::runif(0,0.9);
	  if (Qs(j,z)+0.02>=tmp && Qs(j,z)-0.02<tmp+0.10 && constraints(j,z)==-1){
	    cnt_js++;
	    break;
	  }
	}
	cnt_c++;
      }while(cnt_js==0 && cnt_c<len);

      if (cnt_c<len){
	for (j=0;j<len;j++){
	  if (Qs(j,z)+0.02>=tmp && Qs(j,z)-0.02<tmp+0.10 && constraints(j,z)==-1 && R::runif(0,1)<0.5){
	    double maxc=max(Qs(j,_));
	    sd=scalingE_Qs(j,0)+zigg.norm()*scale;
	    
	    if(sd>maxc)
	      sd=maxc;
	    if (sd<1e-5)
	      sd=1e-5;
	    
	    sd2=param2bin(sd);
	    cnt2++;
	    
	    for (z=0; z<K; z++)
	      scaling_Qs(j,z)=sd2;
	  }else{
	    for (z=0; z<K; z++)
	      scaling_Qs(j,z)=-251;
	  }
	}
      }
    }
	
    if (cnt2 == 0){
      unsigned int j2;
      unsigned int cnt_c=0;
      do{
	j2=std::floor(R::runif(0,len));
	cnt_c++;
      }while(anyTwo(constraints(j2,_),K)==0 && cnt_c<len);

      if (cnt_c<len){
	sd=R::runif(1e-5,1e-1);
	for (z=0; z<K; z++)
	   scaling_Qs(j2,z)=param2bin(sd);
	
	for (j=0;j<len;j++){
	  if (j !=j2){
	    for (z=0; z<K; z++){
	      scaling_Qs(j,z)=-251;
	    }
	  }
	}
      }
    }

    cnt_ml++;
    if (cnt_ml>=2*K*len || (i > 2*K*len && ml-ilik<50 ))
      convergence=1;
    
    if (cnt == thinin){
      if (cnt_scale>75){
        scale=R::runif(0.00001,0.05);
        cnt_scale=0;
      }else{
        scale=accept2scale(accept/(double)thinin,scale,1e-5, 0.25);
        cnt_scale++;
      }
      accept=0;
      cnt=0;
      //      Rcout << ml << " : " << lik << " \n" << Qs << "\n"; 
    }
    cnt++;
    i++;
  }
  return bestQs;

}

NumericVector ReScale (NumericMatrix out, unsigned int record, double scale, unsigned int K, unsigned int len, NumericMatrix constraints){
  NumericVector rescaled(2);
  unsigned int last=record;
  unsigned int i,j,z;
  unsigned int cnt_index_start=K*K+2;
  unsigned int cnt_index;
  double changed_i_q=0;
  double tmp;
  if (last>10)
    last=10;

  int tot=0;
  int tot2=0;
  int tot3=0;
  for (i = record;i>record-last;i--){
    cnt_index=cnt_index_start;
    for (j=0;j<len;j++){
      int b=0;
      for (z=0; z<K; z++){
	tmp=std::fabs(out(i,cnt_index)-out(i-1,cnt_index));
	if (tmp>=1e-8){
	  changed_i_q+=tmp;
	  if (b==0){
	    tot++;
	      b=1;
	  }
	  tot2++;
	}
	cnt_index++;
      }
      if (anyTwo(constraints(j,_),K)==1)
	tot3++;
    }
  }
  
  if (tot3>0 && tot>0){
    rescaled[1]=tot/(double)tot3;
  }else{
    rescaled[1]=R::runif(1e-2,0.125);
  }
  if (tot2>0 && changed_i_q>0){
    rescaled[0]=changed_i_q/(double)tot2;
  }else{
    rescaled[0]=R::runif(1e-8,1e-4);
  }
  return (rescaled);
}

// [[Rcpp::export]]
NumericMatrix mcmc2 (NumericMatrix freq, NumericMatrix Qs, unsigned long int burnin, unsigned int thinin, unsigned long int iter, IntegerMatrix posnames, NumericMatrix dsub, unsigned int K, unsigned int len, NumericMatrix constraints, bool Beta){

  unsigned int j,z;
  unsigned long int i;
  unsigned int cnt=0;
  unsigned int record=0;
  double lik;
  unsigned int nparam=K*K + K*len;
  //  unsigned int toRecord=(int)(iter-burnin)/thinin-1;
  unsigned int toRecord=(int)iter/thinin-1;

  NumericMatrix out(toRecord, nparam+2);

  double tochange=1;
  double minToChange=1/(double)len;
  double maxToChange=1;
  if (maxToChange>1)
    maxToChange=1;

  double llimit1=1e-10;

  unsigned int accept=0;
  double scale=1e-8;
  NumericVector scaled(2);
  scaled[0]=1e-4;
  scaled[1]=0.125;

  NumericMatrix scaling_Frq(K,K);
  NumericMatrix scalingE_Frq(K,K);
  for (i=0;i<K;i++){
    for (j=0;j<K;j++){
      scaling_Frq(i,j)=1e-8;
      scalingE_Frq(i,j)=1e-8;
    }
  }

  IntegerMatrix scaling_Qs(len,K);
  NumericMatrix scalingE_Qs(len,K);
  for (i=0;i<len;i++){
    for (j=0;j<K;j++){
      scaling_Qs(i,j)=-5;
      scalingE_Qs(i,j)=0.5;
    }
  }
  
  NumericMatrix Ds=DistanceAncestries(freq, Qs,  K, len);
  //  NumericMatrix RestDs=CalculateRest(Ds,len);
  unsigned int long s=(unsigned int long)dsub.nrow();
  lik=mdistNoErrFast(Ds, posnames, dsub,s, Beta);

  NumericMatrix freq_n(K,K);
  NumericMatrix Qs_n(len,K);
  double lik_n;
  NumericVector proposals_Ds(2);
  NumericVector proposals_Qs(2);
  double ratio;
  double sd;
  int sd2;
  unsigned int b;
  double a_rate;
  unsigned int cnt_index;
  unsigned int cnt_scale=0;
  unsigned int stop=5*K*K*len;
  for (i=1;i<=iter;i++){
    freq_n=GaussianMutationMH_Ds2(freq,scaling_Frq, K);
    if (R::runif(0,1)<0.01 && i<burnin && i<stop){
      Qs_n=mcmc3(freq_n,Qs, 0, len, 1000,posnames, dsub,K,len,constraints, Beta);
      proposals_Qs[0]=0;
      proposals_Qs[1]=0;
    }else{
      Qs_n=GaussianMutationMH_Qs(Qs,scaling_Qs, K, len, constraints);
      proposals_Qs=GetProposals_Qs(Qs, Qs_n, scaling_Qs, K, len, constraints);
    }

    proposals_Ds=GetProposals_Ds2(freq, freq_n, scaling_Frq, K);
    
    Ds=DistanceAncestries(freq_n, Qs_n, K, len);
    lik_n=mdistNoErrFast(Ds,posnames, dsub,s, Beta);

    ratio=lik_n + proposals_Ds[0] + proposals_Qs[0] - lik - proposals_Ds[1] - proposals_Qs[1];

    // if (ratio>-1000){
    //   Rcpp::Rcout.precision(12);    
    //   Rcout << "===================\n" << std::fixed << lik_n << " + " << proposals_Ds[0] << " + " << proposals_Qs[0] << " - " << lik << " - " << proposals_Ds[1] << " - "  << proposals_Qs[1] << " = " << ratio << "\n\n";
    //   Rcout << freq << "\n" <<  freq_n << "\n";
    //   Rcout << Qs << "\n\n" << Qs_n << "\n";
    //   Rcout << scaling_Qs << "\n";
    // }

    b=0;
    if (ratio>=0){
      b=1;
    }else{ 
      if (std::log(R::runif(0,1))<=ratio)
	b=2;
    }

    if (b>0){
      if (b==1){
	    for (j=0;j<K;j++){
	      for (z=j; z<K; z++)
	        scalingE_Frq(j,z)=std::fabs(freq_n(j,z)-freq(j,z));
	    }
	    for (j=0;j<len;j++){
	      NumericVector diff(K);
	      for (z=0; z<K; z++)
	        diff[z]=Qs_n(j,z)-Qs(j,z);
          double maxc=max(diff);
          double maxz=max(Qs_n(j,_));
          if (maxc>maxz)
            maxc=maxz;
          for (z=0; z<K; z++)
            scalingE_Qs(j,z)=maxc;
        }
      }else{
        for (j=0;j<len;j++){
          for (z=0; z<K; z++)
            scalingE_Qs(j,z)=1e-6;
        }
        for (j=0;j<K;j++){
          for (z=j; z<K; z++)
            scalingE_Frq(j,z)=1e-10;
        }
      }

      freq=freq_n;
      Qs=Qs_n;
      lik=lik_n;
      if ((b ==1 && ratio>1e-6) || (b==2 && ratio<(-1e-6)))
	accept++;
    }

    //    if (b != 1){
      for (j=0; j<K; j++){
	for (z=j; z<K; z++){	  
	  scaling_Frq(j,z)=scalingE_Frq(j,z) + zigg.norm()*scale;
	  if (scaling_Frq(j,z)<1e-10)
	    scaling_Frq(j,z)=1e-10;
	}
      }

      unsigned int cnt2=0;
      tochange=R::rexp(scaled[1]);
      if (tochange<minToChange){
	tochange=minToChange;
      }else if (tochange>maxToChange){
	tochange=maxToChange;
      }
      double tmp=R::runif(0,1);
      if (tmp<0.5){
	for (j=0;j<len;j++){
	  if (R::runif(0,1)<tochange && anyTwo(constraints(j,_),K)==1){
	    sd=scalingE_Qs(j,0) + zigg.norm()*scaled[0];
	    double maxc=max(Qs(j,_));
	    if(sd>maxc)
	      sd=maxc;
	    if (sd<1e-4)
	      sd=1e-4;
	    
	    sd2=param2bin(sd);
	    cnt2++;
	    for (z=0; z<K; z++)
	      scaling_Qs(j,z)=sd2;
	  }else{
	    for (z=0; z<K; z++)
	      scaling_Qs(j,z)=-251;
	  }
	}
      }else if (tmp<0.9){
	unsigned int cnt_c=0;
        do{
          z=std::floor(R::runif(0,K));
	  cnt_c++;
        }while(anyTwo(constraints(_,z),len)==0 && cnt_c<K);

	cnt_c=0;
	double tmp;
	unsigned int cnt_js;
	do{
	  cnt_js=0;
	  for (j=0;j<len;j++){
	    tmp=R::runif(0,0.9);
	    if (Qs(j,z)+0.02>=tmp && Qs(j,z)-0.02<tmp+0.10 && constraints(j,z)==-1){
	      cnt_js++;
	      break;
	    }
	  }
	  cnt_c++;
	}while(cnt_js==0 && cnt_c<len);

	if (cnt_c<len){
	  for (j=0;j<len;j++){
	    if (Qs(j,z)+0.02>=tmp && Qs(j,z)-0.02<tmp+0.10 && constraints(j,z)==-1 && R::runif(0,1)<0.5){
	      double maxc=max(Qs(j,_));
	      sd=scalingE_Qs(j,0)+zigg.norm()*scaled[0];
	      
	      if(sd>maxc)
		sd=maxc;
	      if (sd<1e-4)
		sd=1e-4;

	      sd2=param2bin(sd);
	      cnt2++;
	      for (z=0; z<K; z++)
		scaling_Qs(j,z)=sd2;
	    }else{
	      for (z=0; z<K; z++)
		scaling_Qs(j,z)=-251;
	    }
	  }
	}
      }

      if (cnt2 == 0 && R::runif(0,1)<0.25){
	unsigned int j2;
	unsigned int cnt_c=0;
	do{
	  j2=std::floor(R::runif(0,len));
	  cnt_c++;
	}while(anyTwo(constraints(j2,_),K)==0 && cnt_c<len);

	if (cnt_c<len){
	  sd=R::runif(1e-4,1e-1);
	  for (z=0; z<K; z++)
	    scaling_Qs(j2,z)=param2bin(sd);
	  
	  for (j=0;j<len;j++){
	    if (j !=j2){
	      for (z=0; z<K; z++){
		scaling_Qs(j,z)=-251;
	      }
	    }
	  }
	}
      }
      //}
    
    if (cnt == thinin){

      a_rate=accept/(double)thinin;
      accept=0;
      if (cnt_scale>75 || record<3){
	scale=R::runif(0.00001,0.005);
	scaled[0]=R::runif(0.01,0.25);
	scaled[1]=R::runif(1/(double)len,0.25);
	cnt_scale=0;
      }else{
	scale=accept2scale(a_rate,scale,llimit1, 0.05);
	scaled=ReScale(out,record-1,scale,K,len,constraints);
	cnt_scale++;
      }

      // record the i sample
      if (record<toRecord){
        out(record,0)=i;
        out(record,1)=lik;
        cnt_index=2;
        for (j=0;j<K;j++){
          for (z=0; z<K; z++){
            out(record,cnt_index)=freq(j,z);
            cnt_index++;
          }
        }
	for (j=0;j<len;j++){
	  for (z=0; z<K; z++){
	    out(record,cnt_index)=Qs(j,z);
	    cnt_index++;
	  }
	}
      }
      cnt_index=0;
      NumericVector PrintParam(nparam);
      for (j=0;j<K;j++){
	for (z=0; z<K; z++)
	  PrintParam[cnt_index++]=freq(j,z);
      }
      
      for (j=0;j<len;j++){
	for (z=0; z<K; z++){
	  PrintParam[cnt_index++]=Qs(j,z);
	}
      }
      
      
      Rcout << "Iter2 = " << i << std::scientific << "; LowLimit1 = " << llimit1 << "; Noise = " << scale << " " << scaled[0] << " " << scaled[1] <<  "; Acceptance = " << a_rate << "; lkl = " << std::fixed  << lik << "; Parameters = " << PrintParam << "\n";
      record++;
	
      cnt=0;
    }
    cnt++;
  }
  return out;

}



  
