use strict;
use warnings;

my $cnt=0;
my $ws=$ARGV[2];
my $start=0;
my %data=();
my $ant=0;
my $chr=0;
my @ids=();
my @groups=();
my %groups_hash=();

(!open(FILE,"<:raw", $ARGV[1])) && do{die;};
while(<FILE>){
    chomp;
    my @line=split(/\s+|\t+/,$_);
    push(@ids,$line[0]);
    push(@groups,$line[5]);
    $groups_hash{$line[5]}++;
}
close(FILE);

my @group_list=keys %groups_hash;
my $header="COUNTS ".join(" ",@group_list)."\n";

(!open(FILE,"<:raw", $ARGV[0])) && do{die;};
while(<FILE>){
    my @line=split(/\s+/,$_,5);
    if ($cnt == 0){
	$start=$line[3];
	$chr=$line[0];
	$data{&SaveByGroup($line[4],\@groups,\@group_list)}++;
    }else{
	if ($line[3]-$start>$ws || $line[0] ne $chr){
	    (!open(OUT,">:raw",$ARGV[3]."$chr.$start-$ant.out_genetic")) && do{die;};
	    print OUT $header;
	    foreach my $pattern (keys %data){
		print OUT $data{$pattern}.$pattern;
	    }
	    close (OUT);
	    %data=();
	    $start=$line[3];
	    $cnt=0 if ($line[0] ne $chr);
	    $chr=$line[0]
	}
	$data{&SaveByGroup($line[4],\@groups,\@group_list)}++;
    }
    $ant=$line[3];
    $cnt++;
}
close(FILE);

(!open(OUT,">:raw",$ARGV[3]."$chr.$start-$ant.out_genetic")) && do{die;};
print OUT $header;
foreach my $pattern (keys %data){
    print OUT $data{$pattern}.$pattern;
}
close (OUT);


#######################################
sub SaveByGroup($$){
    my ($line2,$groups,$gl)=@_;
    my @line=split(/\s+/,$line2);
    my %vars=();
    for (0..$#line){
	next if ($line[$_] eq "0");
	$vars{$line[$_]}++;
    }
    my @nucls=keys %vars;

    next if (scalar(@nucls)!=2);
    my $major=$nucls[0];
    my $minor=$nucls[1];
    if ($vars{$nucls[0]}<$vars{$nucls[1]}){
	$major=$nucls[1];
	$minor=$nucls[0];
    }

    my $cnt=0;
    my %counts=();
    for (0..$#line){
	$cnt=int($_/2);
	next if ($line[$_] eq "0");
	if ($line[$_] eq $major){
	    $counts{$$groups[$cnt]}[0]++;
	}else{
	    $counts{$$groups[$cnt]}[1]++;
	}
    }
    my $str="";
    foreach (@$gl){
	$counts{$_}[0] = 0 if (!exists($counts{$_}[0]));
	$counts{$_}[1] = 0 if (!exists($counts{$_}[1]));
	if ($ARGV[4] eq "0"){
	    $str.=" ".$counts{$_}[0].",".$counts{$_}[1];
	}else{
	    $counts{$_}[0]*=0.5;
	    $counts{$_}[1]*=0.5;
	    $str.=" ".int($counts{$_}[0]).",".int($counts{$_}[1]);
	}
    }
    return $str."\n";
}
